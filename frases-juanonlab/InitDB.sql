CREATE TABLE IF NOT EXISTS `sayings` (
  `saying_id` bigint(5) NOT NULL AUTO_INCREMENT,
  `author_saying` varchar(200) NOT NULL,
  `author` varchar(100) NOT NULL,
  `create_at` DATE,
  PRIMARY KEY (`saying_id`)
)


INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (1, 'La felicidad es darse cuenta que nada es demasiado importante', 'Antonio Gala', '2018-11-19');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (2, 'Lo que quiere el sabio, lo busca en sí mismo; el vulgo, lo busca en los demás.', 'Confucio', '2018-08-02');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (3, 'Hay dos clases de hombres: quienes hacen la historia y quienes la padecen', 'Camilo José Cela', '2017-08-02');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (4, 'El amor perfecto es una amistad con momentos eróticos', 'Antonio Gala', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (5, 'El que no ama siempre tiene razón: es lo único que tiene', 'Antonio Gala', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (6, ' Se puede quitar a un general su ejército, pero no a un hombre su voluntad', 'Confucio', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (7, 'Un hombre de virtuosas palabras no es siempre un hombre virtuoso', 'Confucio', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (8, 'Antes de empezar un viaje de venganza cava dos tumbas', 'Confucio', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (9, 'No debemos tener miedo a la muerte, sino a vivir sin intensidad', 'Albert Espinosa', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (10, 'Sólo le falta el tiempo a quien no sabe aprovecharlo', 'Jovellanos', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (11, 'Nunca sabes lo fuerte que eres, hasta que ser fuerte es la única opción que te queda', 'Bob Marley', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (12, '¿No es la vida cien veces demasiado breve para aburrirnos?', 'Jovellanos', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (13, 'Pensamientos tontos los que tenemos todos, pero el sabio se los calla', 'Wilhelm Bush', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (14, 'Un hoy vale por dos mañanas', 'Benjamín Franklin', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (15, 'No cuentes los días, haz que los días cuenten', 'Muhamed Alí', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (16, 'Hijo mío, la felicidad está hecha de pequeñas cosas, un pequeño yate, una pequeña mansión, una pequeña fortuna', 'Groucho Marx', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (17, 'No estoy en este mundo para cumplir con tus expectativas y tú no estás en este mundo para cumplir las mías', 'Bruce Lee', '2018-10-18');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (18, 'Aquel que conoce todas las respuestas no se ha hecho todas las preguntas', 'Confucio', '2018-10-18'); 


