package com.saying.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SayingJuanonlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(SayingJuanonlabApplication.class, args);
	}
}
