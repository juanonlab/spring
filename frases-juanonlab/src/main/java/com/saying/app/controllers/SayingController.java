package com.saying.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.saying.app.models.service.ISayingService;

@Controller
public class SayingController {
	
	@Value("${application.controllers.title}")
	private String title;
	
	
	@Autowired
	private ISayingService sayingService;
	
	@GetMapping("/")
	public String getAllSayings(Model model) {
		model.addAttribute("jl_sayingListTitle", title);
		// call getAllSayings(). If you want fetch all the sayings
		// model.addAttribute("jl_sayingListData", sayingService.getAllSayings());
		model.addAttribute("jl_sayingListData", sayingService.getSayingsRandom());
		return "index";			
	}	
	
	@GetMapping("/bySaying/{id}")
	public String getSaying(Model model, @PathVariable int id) {
		model.addAttribute("jl_sayingListTitle", title);
		model.addAttribute("jl_sayingListData", sayingService.getSayingById(id));
		return "index";			
	}
	
	@GetMapping("/byAuthor/{author}")
	public String getAllSayingsByAuthor(Model model, @PathVariable String author) {
		model.addAttribute("jl_sayingListTitle", title);
		model.addAttribute("jl_sayingListData", sayingService.getSayingsByAuthor(author));
		return "index";			
	}	
	
	@GetMapping("/random")
	public String getAllRandom(Model model) {
		model.addAttribute("jl_sayingListTitle", title);
		model.addAttribute("jl_sayingListData", sayingService.getSayingsRandom());
		return "index";			
	}
	
	@GetMapping("/poweredBy")
	public String getTechnologies(Model model) {
		model.addAttribute("jl_sayingListTitle", title);
		return "poweredBy";			
	}

}
