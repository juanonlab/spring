package com.saying.app.models.dao;

import java.util.List;

import com.saying.app.models.entity.Saying;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ISayingDao extends CrudRepository<Saying, Long> {

    @Query("SELECT a FROM Saying a WHERE a.author=:author")
    List<Saying> fetchSayingForAuthor(@Param("author") String author);
    
}
