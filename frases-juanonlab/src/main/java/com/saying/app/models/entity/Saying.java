package com.saying.app.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="sayings")
public class Saying implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="saying_id")
	private long sayingId;
	
	@Column(name="author_saying")
	private String authorSaying;
	
	@Column(name="author")
	private String author;
	
	@Column(name="create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;
	
			
	public long getSayingId() {
		return sayingId;
	}
	public void setSayingId(long sayingId) {
		this.sayingId = sayingId;
	}
	public String getAuthorSaying() {
		return authorSaying;
	}
	public void setAuthorSaying(String authorSaying) {
		this.authorSaying = authorSaying;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

}
