package com.saying.app.models.service;


import java.util.List;

import com.saying.app.models.entity.Saying;


public interface ISayingService {
	
     List<Saying> getAllSayings();
     
     Saying getSayingById(long sayingId);
     
     boolean addSaying(Saying saying);
     
     void updateSaying(Saying saying);
     
     void deleteSaying(int sayingId);
     
     List<Saying> getSayingsByAuthor(String author);
     
     List<Saying> getSayingsRandom();
          
} 