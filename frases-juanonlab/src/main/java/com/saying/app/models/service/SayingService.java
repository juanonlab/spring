package com.saying.app.models.service;


import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.saying.app.models.entity.Saying;
import com.saying.app.repository.SayingRepository;

@Service
public class SayingService implements ISayingService {
	
	@Autowired
	private SayingRepository sayingRepository;
	
	@Override
	public Saying getSayingById(long sayingId) {
		
		Saying obj = sayingRepository.findById(sayingId).get();
		return obj;
	}	
	
	@Override
	public List<Saying> getAllSayings(){
		List<Saying> list = new ArrayList<>();
		sayingRepository.findAll().forEach(e -> list.add(e));
		return list;
	}
	
	@Override
	public synchronized boolean addSaying(Saying saying){
	        List<Saying> list = sayingRepository.findByAuthorAndAuthorSaying(saying.getAuthor(), saying.getAuthorSaying()); 	
                if (list.size() > 0) {
    	           return false;
                } else {
    	        sayingRepository.save(saying);
    	        return true;
       }
	}
	
	@Override
	public void updateSaying(Saying saying) {
		sayingRepository.save(saying);
	}
	
	@Override
	public void deleteSaying(int sayingId) {
		sayingRepository.delete(getSayingById(sayingId));
	}

	@Override
	public List<Saying> getSayingsByAuthor(String author) {
		List<Saying> sayingByAuthor = sayingRepository.findByAuthor(author);
		return sayingByAuthor;		
	}

	@Override
	public List<Saying> getSayingsRandom() {
		List<Saying> sayingRandom = sayingRepository.findRandom();
		return sayingRandom;
	}

} 