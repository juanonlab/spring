package com.saying.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.saying.app.models.entity.Saying;

public interface SayingRepository extends CrudRepository<Saying, Long>  {
	
    List<Saying> findByAuthor(String author);
    
    List<Saying> findByAuthorAndAuthorSaying(String author, String saying);
         
    @Query(value = "SELECT * FROM sayings ORDER BY RAND() LIMIT 3", nativeQuery = true)
    List<Saying> findRandom();
   
} 