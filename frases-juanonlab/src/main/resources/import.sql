/* Populate table */
/* 
CREATE TABLE IF NOT EXISTS `sayings` (
  `saying_id` bigint(5) NOT NULL AUTO_INCREMENT,
  `author_saying` varchar(200) NOT NULL,
  `author` varchar(100) NOT NULL,
  `create_at` DATE,
  PRIMARY KEY (`saying_id`)
)


INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (1, 'La felicidad es darse cuenta que nada es demasiado importante', 'Antonio Gala', '2018-11-19');

INSERT INTO sayings (saying_id, author_saying, author, create_at) VALUES (2, 'Lo que quiere el sabio, lo busca en sí mismo; el vulgo, lo busca en los demás.', 'Confucio', '2018-08-02');

INSERT INTO sayings (author_saying, author, create_at) VALUES ('Me gusta la musica', 'juan', '2017-08-02'); 
*/
